const {db} = require('./db')
const Schema = db.Schema

const Transaksi = new Schema({
  nama: String,
  kuantitas: Number,
  date: { type: Date, default: Date.now },
  total: Number,
  owner:{
    type:Schema.Types.ObjectId,
    ref:'userData'
  }
});

module.exports = db.model( 'transaksiData', Transaksi, 'transaksiData');