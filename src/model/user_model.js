const {db, passportLocalMongoose} = require('./db')
const Schema = db.Schema

const User = new Schema({
  username: String,
  password: String,
  email: String,
  role: {
    type:String,
    enum:['user', 'admin']
  },
});

User.plugin(passportLocalMongoose);

module.exports = db.model('userData', User, 'userData');