const db = require('mongoose');
const passportLocalMongoose = require('passport-local-mongoose');
db.connect('mongodb://localhost:27017/users',{
  useNewUrlParser: true,
  useUnifiedTopology: true
});

module.exports = {db, passportLocalMongoose}