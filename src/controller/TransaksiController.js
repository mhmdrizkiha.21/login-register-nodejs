const { validationResult } = require('express-validator');
const TransaksiModel = require('../model/transaksi_model');



async function showTransaksi(req, res){
    const transactions = (req.user.role==="admin")?await TransaksiModel.find({}).populate("owner"):await TransaksiModel.find({"owner":req.user.id})
    res.render("transaksi", {transactions: transactions, message:req.flash("message")})
}

async function showDetailTransaksi(req, res){
    const transaction = await TransaksiModel.findById(req.params.id);
    res.render("detail-transaksi", {transaction:transaction, message:req.flash("message")})
}

function createTransaksi(req, res){
    data = {
        nama: req.body.nama_transaksi,
        kuantitas: req.body.kuantitas,
        total: req.body.total,
        date: Date.now(),
        owner:req.user._id
    }

    TransaksiModel.collection.insertOne(data)
    req.flash('message', '<div class="alert alert-success">Transaksi has been created.</div>')
    res.redirect("/transaksi")
}

async function updateTransaksi(req, res){
        data = {
            nama: req.body.nama_transaksi,
            kuantitas: req.body.kuantitas,
            total: req.body.total,
        }
        const updatedTransaksi = await TransaksiModel.updateOne({_id:req.params.id}, data);
        req.flash('message', '<div class="alert alert-success">Transaksi has been updated.</div>')
        res.redirect("/transaksi/"+ req.params.id)
}

async function deleteTransaksi(req, res){
    const cekId = await TransaksiModel.findById(req.params.id);
    if(!cekId) return res.render("404");
    try {
        const deletedTransaksi = await TransaksiModel.deleteOne({_id: req.params.id});
        req.flash('message', '<div class="alert alert-success">Transaksi has been deleted.</div>')
        res.redirect("/transaksi")
    } catch (error) {
        res.render("404");
    }
}

module.exports = {showTransaksi, createTransaksi, deleteTransaksi, showDetailTransaksi, updateTransaksi}