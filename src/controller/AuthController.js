const { validationResult } = require('express-validator');
const UserModel = require('../model/user_model');

function showLogin(req, res){
    let message_login = req.session.messages
    req.session.messages = []
    res.render('login', {message: req.flash("message"), message_login: message_login})
}

function showDashboard(req, res){
    res.render('dashboard')
}

function showRegister(req, res){
    res.render('register')
}

function login(req, res){
    UserModel.findOne({username:req.body.username}, function(err, user){
        if(user.role == "admin"){
            res.redirect("/dashboard");
        }else{
            res.redirect("/transaksi");
        }
    })   
}

function register(req, res){
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        return res.status(422).json({ errors: errors.array() });
    }

    data = {
        username: req.body.username, 
        active: false, 
        email: req.body.email,
        role: "user",
    }

    UserModel.register(data, req.body.password)

    res.redirect("/login");
    
}

function logout(req, res, next){
    req.logout(function(err) {
        if (err) { return next(err) }
        req.flash('message', 'You are now logged out.')
        res.redirect('/login')
      })
}

module.exports = {showLogin, showRegister, login, register, showDashboard, logout}