const express = require('express')
const auth_controller = require('./controller/AuthController')
const transaksi_controller = require('./controller/TransaksiController')
const connectEnsureLogin = require('connect-ensure-login'); //authorization
const passport = require('passport');  // authentication
const isAdmin = require('./middleware/auth')

let router = express.Router()
// validation
const { check } = require('express-validator');
var registerValidate = [
  check('username').trim().escape(),
  check('email', 'Must Be an Email Address').isEmail().trim().escape().normalizeEmail(),
  
  check('password').isLength({ min: 8 })
  .withMessage('Password Must Be at Least 8 Characters')
  .matches('[0-9]').withMessage('Password Must Contain a Number')
  .matches('[A-Z]').withMessage('Password Must Contain an Uppercase Letter').trim().escape()];

// route
router.get("/", (req, res) => {
  res.send(`Hello 
  <a href="/login">Log In</a><br><br>`);
})

// auth
router.get("/login", auth_controller.showLogin)
router.get("/register", auth_controller.showRegister)
router.post("/login", passport.authenticate('local', { failureRedirect: '/login' , failureMessage: "invalid username or password"}), auth_controller.login)
router.post("/register", registerValidate, auth_controller.register)
router.get("/dashboard", connectEnsureLogin.ensureLoggedIn(), isAdmin, auth_controller.showDashboard);
router.get("/logout", auth_controller.logout);

// transaksi
router.get("/transaksi", connectEnsureLogin.ensureLoggedIn(), transaksi_controller.showTransaksi)
router.get("/transaksi/:id", connectEnsureLogin.ensureLoggedIn(), transaksi_controller.showDetailTransaksi)
router.post("/transaksi", connectEnsureLogin.ensureLoggedIn(), transaksi_controller.createTransaksi)
router.put("/transaksi/:id", connectEnsureLogin.ensureLoggedIn(), transaksi_controller.updateTransaksi)
router.delete("/transaksi/:id", connectEnsureLogin.ensureLoggedIn(), transaksi_controller.deleteTransaksi)

router.get("*", (req, res) => {
  res.render("404")
})

module.exports = router