# APLIKASI REGISTER DAN LOGIN 

Menggunakan teknologi Node JS, Express, MongoDB dan Template coreUI

## Profile

-   Nama : Muhammad Rizki Halomoan
-   NIM : 211511049
-   kelas : 2B


### Route
1. GET '/' : Halaman utama
2. GET '/login' : Halaman login
3. GET '/register' : Halaman Register
4. GET '/dashboard' : Halaman Dashboard
5. POST '/login' : proses login
6. POST '/register' : proses register
7. GET '/logout' : proses logout


