var express = require('express')
var app     = express()
const path = require('path')
const router = require('./src/routes')
const bodyParser = require('body-parser')
const passport = require('passport')  // authentication
const User = require('./src/model/user_model') // User Model 
const LocalStrategy = require("passport-local")
const flash = require('connect-flash');
const cors = require('cors')
var cookieParser = require('cookie-parser');
var methodOverride  = require("method-override");
//session 
const session = require('express-session'); // express-sessions
const { v4: uuidv4 } = require('uuid'); // uuid, To call: uuidv4();


// body parser
app.use(bodyParser.urlencoded({ extended: false }));
// directory for hbs
const publicDirectoryPath = path.join(__dirname, "views");
app.use(express.static(publicDirectoryPath));
app.set('view engine', 'html')
app.engine("html",require("hbs").__express)
const viewsPath = path.join(__dirname, 'views')
app.set('views', viewsPath)


// session
app.use(session({
  genid: function(req){
    return uuidv4();
  },
  secret: '=fmLV*U@FL`N]]~/zqtFCch.pBTGoU',
  resave: false,
  saveUninitialized: true,
  cookie: { maxAge: 60 * 60 * 1000 }
}))
// cookie
app.use(cookieParser('secret'));

// flash
app.use(flash())

//passport
app.use(passport.initialize());
app.use(passport.session());
//setting passport
passport.use(new LocalStrategy(User.authenticate()));
// To use with sessions
passport.serializeUser((user, done) => {
  done(null, user.id);
})

passport.deserializeUser(async (id, done) => {
  const user = await User.findById(id);
  if (!user) {
    done(error, false);
  }
  done(null, user);
})


// use route
app.use(cors())
app.use(router)
// run server
const PORT = process.env.PORT || 3000
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`)
})
